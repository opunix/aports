# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=wt
pkgver=4.6.2
pkgrel=0
pkgdesc="C++ library and application server for developing and deploying web applications"
url="https://www.webtoolkit.eu/"
arch="all"
license="GPL-2.0-only WITH openssl-exception"
depends_dev="zlib-dev boost-dev sqlite-dev mesa-dev glu-dev graphicsmagick-dev
	openssl1.1-compat-dev pango-dev fcgi-dev libpq-dev qt5-qtbase-dev libharu-dev
	harfbuzz-dev"
makedepends="$depends_dev cmake boost"
pkggroups="wt"
pkgusers="wt"
options="!check"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/emweb/wt/archive/$pkgver.tar.gz"

build() {
	CXXFLAGS="$CXXFLAGS -fpermissive" \
	cmake -B build . \
		-DCMAKE_BUILD_TYPE=None \
		-DCONNECTOR_HTTP=ON \
		-DWT_WRASTERIMAGE_IMPLEMENTATION=GraphicsMagick \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWEBUSER=$pkgusers \
		-DWEBGROUP=$pkggroups \
		-DRUNDIR="$pkgdir"/var/run/wt \
		-DUSE_SYSTEM_SQLITE3=ON \
		-DINSTALL_EXAMPLES=ON \
		-DBUILD_EXAMPLES=OFF
	make -C build
}

package() {
	make -C build DESTDIR=$pkgdir install
	rm -rf $pkgdir/usr/cmake
	rm -rf $pkgdir/var/run
}

sha512sums="
0201c7b7a0da42d80b8a66fdff9ab0ce715960f03551ebabcac09f96240c39d2b325fc12c676d74023fddb7a024e35dde8162775279f7e55bb34c3f99a0d7bc1  wt-4.6.2.tar.gz
"
